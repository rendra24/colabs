<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'web';
$route['checkout'] = 'web/checkout';
$route['history'] = 'web/history';
$route['account'] = 'web/account';
$route['pembayaran/(:any)/(:any)'] = 'web/pembayaran/$1/$2';
$route['masuk'] = 'auth/index';
$route['daftar'] = 'auth/daftar';
$route['mobile-development'] = 'web/detail/2';
$route['web-development'] = 'web/detail/1';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
