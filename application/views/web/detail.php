<!--====== SERVICES PART START ======-->

<section id="features" class="services-area pt-120">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-10">
                <div class="section-title text-center pb-40">
                    <div class="line m-auto"></div>
                    <h3 class="title">Harga</h3>
                </div> <!-- section title -->
            </div>
        </div> <!-- row -->
        <div class="row justify-content-center">
            <?php 

                $produk = $this->db->get_where('produk', array('id_kategori' => $id))->result_array();

                foreach ($produk as $row) {
            ?>
                  <div class="col-lg-4 col-md-7 col-sm-8">
                <div class="single-services text-center mt-30 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s" style="min-height: 571px;">
                    <div class="services-icon">
                        <img class="shape" src="<?php echo base_url().'assets/images/services-shape.svg';?>" alt="shape">
                        <img class="shape-1" src="<?php echo base_url().'assets/images/services-shape-new.svg';?>" alt="shape">
                        <i class="lni lni-code"></i>
                    </div>
                    <div class="services-content mt-30">
                        <h4 class="services-title" style="color: #0065FF;"><?php echo $row['nama_produk']; ?></h4><br>
                        <div style="min-height: 194px;"><?php echo $row['keterangan']; ?></div><br>

                         <h4 class="services-title"><a href="#"><?php echo $row['harga']; ?></a></h4><br>
                        <a href="https://api.whatsapp.com/send?phone=6285156721183&text=Halo saya ingin bertanya seputar teknologi digital&source=&data=&app_absent=" class="main-btn wow fadeInUp" data-wow-duration="1.3s" data-wow-delay="1.1s">Kontak Kami</a>
                    </div>
                </div> <!-- single services -->
            </div>
            <?php
                }
             ?>
          
          
        </div> <!-- row -->
    </div> <!-- container -->
</section>

<!--====== SERVICES PART ENDS ======-->












<!--====== FOOTER PART START ======-->

<footer id="footer" class="footer-area pt-120">
    <div class="container">
        <div class="subscribe-area wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s" style="padding-bottom: 40px;">
            <div class="row">
            <div class="col-lg-12">
                <div class="brand-logo d-flex align-items-center justify-content-center justify-content-md-between">
                 <div class="slick-carosel" style="width: 100%;">

                    <div class="single-logo mt-30 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s" style="text-align: center;">
                        <img src="<?php echo base_url().'assets/android-studio.png';?>" alt="brand" style="height: auto;width: 75px;">
                    </div> 

                    <div class="single-logo mt-30 wow fadeIn" data-wow-duration="1.5s" data-wow-delay="0.2s">
                        <img src="<?php echo base_url().'assets/codeigniter.png';?>" alt="brand" style="height: auto;width: 185px;margin-top: 25px;">
                    </div> 

                    <div class="single-logo mt-30 wow fadeIn" data-wow-duration="1.5s" data-wow-delay="0.3s">
                        <img src="<?php echo base_url().'assets/laravel.png';?>" alt="brand" style="height: auto;width: 195px;margin-top: 25px;">
                    </div> 

                    <div class="single-logo mt-30 wow fadeIn" data-wow-duration="1.5s" data-wow-delay="0.4s">
                        <img src="<?php echo base_url().'assets/framework7.png';?>" alt="brand" style="height: auto;width: 175px;margin-top: 8px;">
                    </div>

                    <div class="single-logo mt-30 wow fadeIn" data-wow-duration="1.5s" data-wow-delay="0.5s">
                        <img src="<?php echo base_url().'assets/git.png';?>" alt="brand" style="height: auto;width: 75px;">
                    </div>

                    <div class="single-logo mt-30 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s">
                        <img src="<?php echo base_url().'assets/figma.png';?>" alt="brand" style="height: auto;width: 185px;margin-top: 7px;">
                    </div> 

                    <div class="single-logo mt-30 wow fadeIn" data-wow-duration="1.5s" data-wow-delay="0.2s">
                        <img src="<?php echo base_url().'assets/react.png';?>" alt="brand" style="height: auto;width: 85px;;">
                    </div> 

                    <div class="single-logo mt-30 wow fadeIn" data-wow-duration="1.5s" data-wow-delay="0.3s">
                        <img src="<?php echo base_url().'assets/blender.png';?>" alt="brand" style="height: auto;width: 90px;">
                    </div> 

                    <div class="single-logo mt-30 wow fadeIn" data-wow-duration="1.5s" data-wow-delay="0.4s">
                        <img src="<?php echo base_url().'assets/nodejs.png';?>" alt="brand" style="height: auto;width: 145px;">
                    </div>

                    <div class="single-logo mt-30 wow fadeIn" data-wow-duration="1.5s" data-wow-delay="0.5s">
                        <img src="<?php echo base_url().'assets/express.png';?>" alt="brand" style="height: auto;width: 195px;margin-top: 25px;">
                    </div>

                </div>
            </div> <!-- brand logo -->
        </div>
    </div>
                
            </div> <!-- subscribe area -->
            <div class="footer-widget pb-100">
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-8">
                        <div class="footer-about mt-50 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s">
                            <a class="logo" href="#">
                                <img src="assets/images/logo.svg" alt="logo">
                            </a>
                            <p class="text">Ciptakan produk hebat dan berkolaborasi</p>
                            <ul class="social">
                                <li><a href="https://github.com/colabs-id"><i class="lni lni-github-original"></i></a></li>
                                <li><a href="https://www.instagram.com/colabs.id/"><i class="lni lni-instagram-filled"></i></a></li>
                                <li><a href="https://www.linkedin.com/company/colabs-indonesia/"><i class="lni lni-linkedin-original"></i></a></li>
                            </ul>
                        </div> <!-- footer about -->
                    </div>
                    <div class="col-lg-3 col-md-7 col-sm-7">
                        <div class="footer-link d-flex mt-50 justify-content-md-between">
                            <!-- <div class="link-wrapper wow fadeIn" data-wow-duration="1s" data-wow-delay="0.4s">
                                <div class="footer-title">
                                    <h4 class="title">Quick Link</h4>
                                </div>
                                <ul class="link">
                                    <li><a href="#">Road Map</a></li>
                                    <li><a href="#">Privacy Policy</a></li>
                                    <li><a href="#">Refund Policy</a></li>
                                    <li><a href="#">Terms of Service</a></li>
                                    <li><a href="#">Pricing</a></li>
                                </ul>
                            </div> --> <!-- footer wrapper -->
                            <div class="link-wrapper wow fadeIn" data-wow-duration="1s" data-wow-delay="0.6s">
                                <div class="footer-title">
                                    <h4 class="title">Resources</h4>
                                </div>
                                <ul class="link">
                                    <li><a href="#">Proyek Kami</a></li>
                                    <li><a href="#">Produk Kami</a></li>
                                    <li><a href="#">Blog</a></li>
                                </ul>
                            </div> <!-- footer wrapper -->
                        </div> <!-- footer link -->
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-5">
                        <div class="footer-contact mt-50 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.8s">
                            <div class="footer-title">
                                <h4 class="title">Kontak Kami</h4>
                            </div>
                            <ul class="contact">
                                <li>+6285156721183</li>
                                <li>info@colabs.id</li>
                                <li>Jl. Simpang Ijen No.2, Oro-oro Dowo, Kec. Klojen, Kota Malang, Jawa Timur 65113</li>
                                <li>Jl.Buring Indah Regency VI, Buring Kec.Kedungkandang, Kota Malang, Jawa Timur 65136</li>
                            </ul>
                        </div> <!-- footer contact -->
                    </div>
                </div> <!-- row -->
            </div> <!-- footer widget -->

        </div> <!-- container -->
        <div id="particles-2"></div>
    </footer>
    
    <!--====== FOOTER PART ENDS ======-->
    
    <!--====== BACK TOP TOP PART START ======-->

    <a href="#" class="back-to-top" style="margin-right: 75px;"><i class="lni lni-chevron-up" style="margin-top: 8px;"></i></a>

    <!--====== BACK TOP TOP PART ENDS ======-->   
    
    <!--====== PART START ======-->
    
<!--
    <section class="">
        <div class="container">
            <div class="row">
                <div class="col-lg-"></div>
            </div>
        </div>
    </section>
-->

<!--====== PART ENDS ======-->


<div style="bottom: 0;right: 0;position: fixed;z-index: 9;">
    <a href="https://api.whatsapp.com/send?phone=6285156721183&text=Halo saya ingin bertanya seputar teknologi digital&source=&data=&app_absent="><img src="<?php echo base_url().'assets/wa.svg' ?>" style="width: 70px;height: 70px;margin-bottom: 15px;margin-right: 15px;"></a>
</div>