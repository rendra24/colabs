<style>
    .engin-mobile{
        margin-top: 25px;
    }

    @media (max-width:480px)  {
       .engin-mobile{
        margin-top: 10px
        }
    }
</style>
    <!--====== BRAMD PART START ======-->
    
    <div class="brand-area pt-90">
        <div class="container">
           <div class="row justify-content-center">
            <div class="col-lg-10">
                <div class="section-title text-center pb-40">
                    <div class="line m-auto"></div>
                    <h3 class="title">Client Kami</h3>
                </div> <!-- section title -->
            </div>
        </div> <!-- row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="brand-logo d-flex align-items-center justify-content-center justify-content-md-between">
                 <div class="slick-carosel" style="width: 100%;">

                    <div class="single-logo mt-30 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s">
                        <img src="<?php echo base_url().'assets/um.png';?>" alt="brand" style="height: auto;width: 185px;">
                    </div> 

                    <div class="single-logo mt-30 wow fadeIn" data-wow-duration="1.5s" data-wow-delay="0.2s">
                        <img src="<?php echo base_url().'assets/disnak.png';?>" alt="brand" style="height: auto;width: 185px;">
                    </div> 

                    <div class="single-logo mt-30 wow fadeIn" data-wow-duration="1.5s" data-wow-delay="0.3s">
                        <img src="<?php echo base_url().'assets/dinsos.png';?>" alt="brand" style="height: auto;width: 195px;">
                    </div> 

                    <div class="single-logo mt-30 wow fadeIn" data-wow-duration="1.5s" data-wow-delay="0.4s">
                        <img src="<?php echo base_url().'assets/bni.png';?>" alt="brand" style="height: auto;width: 175px;">
                    </div>

                    <div class="single-logo mt-30 wow fadeIn" data-wow-duration="1.5s" data-wow-delay="0.5s">
                        <img src="<?php echo base_url().'assets/polres.png';?>" alt="brand" style="height: auto;width: 75px;margin-top: -10px;">
                    </div>

                    <div class="single-logo mt-30 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s">
                        <img src="<?php echo base_url().'assets/karirpad.png';?>" alt="brand" style="height: auto;width: 195px">
                    </div> 

                    <div class="single-logo mt-30 wow fadeIn" data-wow-duration="1.5s" data-wow-delay="0.2s">
                        <img src="<?php echo base_url().'assets/smkcokro.png';?>" alt="brand" style="height: auto;width: 195px">
                    </div> 

                    <div class="single-logo mt-30 wow fadeIn" data-wow-duration="1.5s" data-wow-delay="0.3s">
                        <img src="<?php echo base_url().'assets/arcegee.png';?>" alt="brand" style="height: auto;width: 195px;">
                    </div> 

                    <div class="single-logo mt-30 wow fadeIn" data-wow-duration="1.5s" data-wow-delay="0.4s">
                        <img src="<?php echo base_url().'assets/topmobil.png';?>" alt="brand" style="height: auto;width: 175px;">
                    </div>

                    <div class="single-logo mt-30 wow fadeIn" data-wow-duration="1.5s" data-wow-delay="0.5s">
                        <img src="<?php echo base_url().'assets/bintangberlian.png';?>" alt="brand" style="height: auto;width: 195px;">
                    </div>

                </div>
            </div> <!-- brand logo -->
        </div>
    </div>   <!-- row -->


</div> <!-- container -->
</div>

<!--====== BRAMD PART ENDS ======-->

<!--====== SERVICES PART START ======-->

<section id="features" class="services-area pt-120">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-10">
                <div class="section-title text-center pb-40">
                    <div class="line m-auto"></div>
                    <h3 class="title">Layanan Kami</h3>
                </div> <!-- section title -->
            </div>
        </div> <!-- row -->
        <div class="row justify-content-center">
            <div class="col-lg-4 col-md-7 col-sm-8">
                <div class="single-services text-center mt-30 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s" style="min-height: 527px;">
                    <div class="services-icon">
                        <img class="shape" src="<?php echo base_url().'assets/images/services-shape.svg';?>" alt="shape">
                        <img class="shape-1" src="<?php echo base_url().'assets/images/services-shape-1.svg';?>" alt="shape">
                        <i class="lni lni-layout"></i>
                    </div>
                    <div class="services-content mt-30">
                        <h4 class="services-title">Website <br> Development</h4>
                        <p class="text" style="min-height: 128px;">Kami dapat membantu pengembangan produk anda dengan memberikan situs web yang berpusat pada skalabilitas keamanan dan performa sistem.</p>
                        <a href="<?php echo base_url().'web-development'; ?>" class="main-btn wow fadeInUp" data-wow-duration="1.3s" data-wow-delay="1.1s" style="background: #172B4D;">Detail</a>
                    </div>
                </div> <!-- single services -->
            </div>
            <div class="col-lg-4 col-md-7 col-sm-8">
                <div class="single-services text-center mt-30 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s" style="min-height: 527px;">
                    <div class="services-icon">
                        <img class="shape" src="<?php echo base_url().'assets/images/services-shape.svg'; ?>" alt="shape">
                        <img class="shape-1" src="<?php echo base_url().'assets/images/services-shape-2.svg'; ?>" alt="shape">
                        <i class="lni lni-mobile" style="left: 46%;"></i>
                    </div>
                    <div class="services-content mt-30">
                        <h4 class="services-title">Mobile <br> Development</h4>
                        <p class="text" style="min-height: 128px;">Kami mengembangkan aplikasi berbasis mobile dengan mengutamakan kenyamanan pengguna.</p>
                        <a href="<?php echo base_url().'mobile-development'; ?>" class="main-btn wow fadeInUp" data-wow-duration="1.3s" data-wow-delay="1.1s" style="background: #172B4D;">Detail</a>
                    </div>
                </div> <!-- single services -->
            </div>
            <div class="col-lg-4 col-md-7 col-sm-8">
                <div class="single-services text-center mt-30 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.8s" style="min-height: 527px;">
                    <div class="services-icon">
                        <img class="shape" src="<?php echo base_url().'assets/images/services-shape.svg'; ?>" alt="shape">
                        <img class="shape-1" src="<?php echo base_url().'assets/images/services-shape-3.svg'; ?>" alt="shape">
                        <i class="lni lni-vector"></i>
                    </div>
                    <div class="services-content mt-30">
                        <h4 class="services-title">Design <br> Services</h4>
                        <p class="text" style="min-height: 128px;">Kami juga melayani pembuatan desain antarmuka (UI/UX),ilustrasi,icon,3D modeling, animasi hingga logo untuk branding bisnis anda.</p>
                           <a href="https://api.whatsapp.com/send?phone=6285156721183&text=Halo saya ingin bertanya layanan jasa desain dari colabs.id&source=&data=&app_absent=" class="main-btn wow fadeInUp" data-wow-duration="1.3s" data-wow-delay="1.1s" style="background: #172B4D;">Detail</a>
                    </div>
                </div> <!-- single services -->
            </div>
        </div> <!-- row -->
    </div> <!-- container -->
</section>

<!--====== SERVICES PART ENDS ======-->





<!--====== ABOUT PART ENDS ======-->

<!--====== VIDEO COUNTER PART START ======-->

<section id="facts" class="video-counter pt-70">
    <div class="container">
<?php
            $tentang = get_konten(1);

             foreach ($tentang as $row) { ?>
        <div class="row">
            <div class="col-lg-6">
                <div class="video-content mt-50 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">
                    <img class="dots" src="<?php echo base_url().'assets/images/dots.svg';?>" alt="dots">
                    <div class="video-wrapper">
                        <div class="video-image">
                            <img src="<?php echo base_url().'assets_foto/'.$row['IMAGE']; ?>" alt="video">
                        </div>

                    </div> <!-- video wrapper -->
                </div> <!-- video content -->
            </div>
            <div class="col-lg-6">
                <div class="counter-wrapper mt-50 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.8s">
                    <div class="counter-content">
                        <div class="section-title">
                            <div class="line"></div>
                            <h3 class="title">Tentang Kami</h3>
                        </div> <!-- section title -->
                        
                        <p class="text" style="text-align: justify;"><?php echo $row['META_DESC']; ?></p>
                   
                    </div> 

                </div> <!-- counter wrapper -->
            </div>
        </div> <!-- row -->
         <?php } ?>
    </div> <!-- container -->
</section>

<!--====== VIDEO COUNTER PART ENDS ======-->

<section id="blog" class="blog-area pt-120">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="section-title pb-35">
                    <div class="line"></div>
                    <h3 class="title">Proyek Kami</h3>
                </div> 
            </div>
        </div> 
        <div class="row justify-content-center">
            <?php
            $proyek = get_konten(3);

             foreach ($proyek as $row) { ?>

                <div class="col-lg-4 col-md-7">
                <div class="single-blog mt-30 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.2s; animation-name: fadeIn;">
                    <div class="blog-image">
                        <img src="<?php echo base_url().'assets_foto/'.$row['IMAGE']; ?>" alt="blog">
                    </div>
                    <div class="blog-content">
                        <h4><?php echo $row['META_TITLE']; ?></h4>
                        <?php if($row['ID'] != 15){ ?>
                        <a class="more" href="<?php echo base_url().'web/detail_proyek/'.$row['ID']; ?>">Lihat Detail <i class="lni lni-chevron-right"></i></a>
                    <?php } ?>
                    </div>
                </div> 
            </div>

            <?php } ?>
            
            <!-- <div class="col-lg-4 col-md-7">
                <div class="single-blog mt-30 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.5s; animation-name: fadeIn;">
                    <div class="blog-image">
                         <img src="<?php echo base_url().'assets/projek-arcegee.svg'; ?>" alt="blog">
                    </div>
                    <div class="blog-content">
                        <h4>Arcigee</h4>
                        <a class="more" href="#">Lihat Detail <i class="lni lni-chevron-right"></i></a>
                    </div>
                </div> 
            </div>
            <div class="col-lg-4 col-md-7">
                <div class="single-blog mt-30 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.8s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.8s; animation-name: fadeIn;">
                    <div class="blog-image">
                         <img src="<?php echo base_url().'assets/projek-growcery.svg'; ?>" alt="blog">
                    </div>
                    <div class="blog-content">
                        <h4>Growcery</h4>
                        <a class="more" href="#">Lihat Detail <i class="lni lni-chevron-right"></i></a>
                    </div>
                </div> 
            </div>
            <div class="col-lg-4 col-md-7">
                <div class="single-blog mt-30 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.8s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.8s; animation-name: fadeIn;">
                    <div class="blog-image">
                         <img src="<?php echo base_url().'assets/projek-tanjung.svg'; ?>" alt="blog">
                    </div>
                    <div class="blog-content">
                        <h4>Learning Apps</h4>
                        <a class="more" href="#">Lihat Detail <i class="lni lni-chevron-right"></i></a>
                    </div>
                </div> 
            </div>
            <div class="col-lg-4 col-md-7">
                <div class="single-blog mt-30 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.8s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.8s; animation-name: fadeIn;">
                    <div class="blog-image">
                         <img src="<?php echo base_url().'assets/polisi.svg'; ?>" alt="blog">
                    </div>
                    <div class="blog-content">
                        <h4>Wadul Polisi</h4>
                        <a class="more" href="#">Lihat Detail <i class="lni lni-chevron-right"></i></a>
                    </div>
                </div> 
            </div>
            <div class="col-lg-4 col-md-7">
                <div class="single-blog mt-30 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.8s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.8s; animation-name: fadeIn;">
                    <div class="blog-image">
                         <img src="<?php echo base_url().'assets/proyek.svg'; ?>" alt="blog">
                    </div>
                    <div class="blog-content">
                        <h4>Proyek Kamu ?</h4>
                        <a class="more" href="#">Lihat Detail <i class="lni lni-chevron-right"></i></a>
                    </div>
                </div> 
            </div> -->
        </div> 
    </div> 
</section>





<!--====== FOOTER PART START ======-->

<footer id="footer" class="footer-area pt-120">
    <div class="container">
        <div class="subscribe-area wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s" style="padding-bottom: 40px;">
            <div class="row">
            <div class="col-lg-12">
                <div class="brand-logo d-flex align-items-center justify-content-center justify-content-md-between">
                 <div class="slick-carosel" style="width: 100%;">

                    <div class="single-logo mt-30 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s" style="text-align: center;">
                        <img src="<?php echo base_url().'assets/android-studio.png';?>" alt="brand" style="height: auto;width: 75px;">
                    </div> 

                    <div class="single-logo mt-30 wow fadeIn" data-wow-duration="1.5s" data-wow-delay="0.2s">
                        <img src="<?php echo base_url().'assets/codeigniter.png';?>" alt="brand" style="height: auto;width: 185px;" class="engin-mobile">
                    </div> 

                    <div class="single-logo mt-30 wow fadeIn" data-wow-duration="1.5s" data-wow-delay="0.3s">
                        <img src="<?php echo base_url().'assets/laravel.png';?>" alt="brand" style="height: auto;width: 195px;" class="engin-mobile">
                    </div> 

                    <div class="single-logo mt-30 wow fadeIn" data-wow-duration="1.5s" data-wow-delay="0.4s">
                        <img src="<?php echo base_url().'assets/framework7.png';?>" alt="brand" style="height: auto;width: 175px;margin-top: 8px;">
                    </div>

                    <div class="single-logo mt-30 wow fadeIn" data-wow-duration="1.5s" data-wow-delay="0.5s">
                        <img src="<?php echo base_url().'assets/git.png';?>" alt="brand" style="height: auto;width: 75px;">
                    </div>

                    <div class="single-logo mt-30 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s">
                        <img src="<?php echo base_url().'assets/figma.png';?>" alt="brand" style="height: auto;width: 185px;margin-top: 7px;">
                    </div> 

                    <div class="single-logo mt-30 wow fadeIn" data-wow-duration="1.5s" data-wow-delay="0.2s">
                        <img src="<?php echo base_url().'assets/react.png';?>" alt="brand" style="height: auto;width: 85px;;">
                    </div> 

                    <div class="single-logo mt-30 wow fadeIn" data-wow-duration="1.5s" data-wow-delay="0.3s">
                        <img src="<?php echo base_url().'assets/blender.png';?>" alt="brand" style="height: auto;width: 90px;">
                    </div> 

                    <div class="single-logo mt-30 wow fadeIn" data-wow-duration="1.5s" data-wow-delay="0.4s">
                        <img src="<?php echo base_url().'assets/nodejs.png';?>" alt="brand" style="height: auto;width: 145px;">
                    </div>
<!-- 
                    <div class="single-logo mt-30 wow fadeIn" data-wow-duration="1.5s" data-wow-delay="0.5s">
                        <img src="<?php echo base_url().'assets/express.png';?>" alt="brand" style="height: auto;width: 195px;" class="engin-mobile">
                    </div> -->

                </div>
            </div> <!-- brand logo -->
        </div>
    </div>
                <!-- <div class="row">
                    <div class="col-lg-6">
                        <div class="subscribe-content mt-45">
                            <h2 class="subscribe-title">Subscribe Our Newsletter <span>get reguler updates</span></h2>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="mt-50">
                            <form action="#">
                                <label>Nama</label>
                                <input type="text" class="form-control"  style="margin-bottom: 15px;height: 45px;">

                                <label>Email</label>
                                <input type="text" class="form-control" style="margin-bottom: 15px;height: 45px;">

                                <label>Nomor Telephone</label>
                                <input type="text" class="form-control"  style="margin-bottom: 15px;height: 45px;">

                                <label>Pesan</label>
                                <textarea class="form-control"  style="border: 2px solid #E1E1E1;height: 65px;padding: 0 30px;border-radius: 5px;width: 100%;color: #2E2E2E;margin-bottom: 15px;"></textarea>

                                <button class="btn btn-info btn-block btn-lg">Kirim</button>
                               
                            </form>
                        </div>
                    </div>
                </div> --> <!-- row -->
            </div> <!-- subscribe area -->
            <div class="footer-widget pb-100">
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-8">
                        <div class="footer-about mt-50 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s">
                            <a class="logo" href="#">
                                <img src="assets/images/logo.svg" alt="logo">
                            </a>
                            <p class="text">Ciptakan produk hebat dan berkolaborasi</p>
                            <ul class="social">
                                <li><a href="https://github.com/colabs-id"><i class="lni lni-github-original"></i></a></li>
                                <li><a href="https://www.instagram.com/colabs.id/"><i class="lni lni-instagram-filled"></i></a></li>
                                <li><a href="https://www.linkedin.com/company/colabs-indonesia/"><i class="lni lni-linkedin-original"></i></a></li>
                            </ul>
                        </div> <!-- footer about -->
                    </div>
                    <div class="col-lg-3 col-md-7 col-sm-7">
                        <div class="footer-link d-flex mt-50 justify-content-md-between">
                            <!-- <div class="link-wrapper wow fadeIn" data-wow-duration="1s" data-wow-delay="0.4s">
                                <div class="footer-title">
                                    <h4 class="title">Quick Link</h4>
                                </div>
                                <ul class="link">
                                    <li><a href="#">Road Map</a></li>
                                    <li><a href="#">Privacy Policy</a></li>
                                    <li><a href="#">Refund Policy</a></li>
                                    <li><a href="#">Terms of Service</a></li>
                                    <li><a href="#">Pricing</a></li>
                                </ul>
                            </div> --> <!-- footer wrapper -->
                            <div class="link-wrapper wow fadeIn" data-wow-duration="1s" data-wow-delay="0.6s">
                                <div class="footer-title">
                                    <h4 class="title">Resources</h4>
                                </div>
                                <ul class="link">
                                    <li><a href="#">Proyek Kami</a></li>
                                    <li><a href="#">Produk Kami</a></li>
                                    <li><a href="#">Blog</a></li>
                                </ul>
                            </div> <!-- footer wrapper -->
                        </div> <!-- footer link -->
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-5">
                        <div class="footer-contact mt-50 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.8s">
                            <div class="footer-title">
                                <h4 class="title">Kontak Kami</h4>
                            </div>
                            <ul class="contact">
                                <li>+6285156721183</li>
                                <li>info@colabs.id</li>
                                <li>Jl. Simpang Ijen No.2, Oro-oro Dowo, Kec. Klojen, Kota Malang, Jawa Timur 65113</li>
                                <li>Jl.Buring Indah Regency VI, Buring Kec.Kedungkandang, Kota Malang, Jawa Timur 65136</li>
                            </ul>
                        </div> <!-- footer contact -->
                    </div>
                </div> <!-- row -->
            </div> <!-- footer widget -->

        </div> <!-- container -->
        <div id="particles-2"></div>
    </footer>
    
    <!--====== FOOTER PART ENDS ======-->
    
    <!--====== BACK TOP TOP PART START ======-->

    <a href="#" class="back-to-top" style="margin-right: 75px;"><i class="lni lni-chevron-up" style="margin-top: 8px;"></i></a>

    <!--====== BACK TOP TOP PART ENDS ======-->   
    
    <!--====== PART START ======-->
    
<!--
    <section class="">
        <div class="container">
            <div class="row">
                <div class="col-lg-"></div>
            </div>
        </div>
    </section>
-->

<!--====== PART ENDS ======-->


<div style="bottom: 0;right: 0;position: fixed;z-index: 9;">
    <a href="https://api.whatsapp.com/send?phone=6285156721183&text=Halo saya ingin bertanya seputar teknologi digital&source=&data=&app_absent="><img src="<?php echo base_url().'assets/wa.svg' ?>" style="width: 70px;height: 70px;margin-bottom: 15px;margin-right: 15px;"></a>
</div>