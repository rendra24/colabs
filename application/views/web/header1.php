<html class="no-js" lang="en">

<head>
	<meta name="google-site-verification" content="AnEHirXghr9GsKm72FM1LHOug9rU7Tgyh1prr8C3jfc" />
    <meta charset="utf-8">
    <meta property="og:locale" content="en_US" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="MobileOptimized" content="width" />
    <meta name="keywords" content="jasa website pasuruan,jasa website malang, website malang, jasa pembuatan website pasuruan, desain website malang, jasa pembuatan website terbaik di pasuruan, jasa pembuatan website murah malang,"/>
    <meta name="robots" content="index,follow" />
   	<meta name="description" content="COLABS INDONESIA Jasa Website dan Aplikasi,Jasa Logo,Jasa Animasi, Jasa UI/UX murah berkualitas"/>
    <meta property="og:type" content="website" />
    <meta property="og:title" content="COLABS INDONESIA - Ciptakan produk hebat dan berkolaborasi" />
    <meta property="og:description" content="COLABS INDONESIA Jasa Website dan Aplikasi,Jasa Logo,Jasa Animasi, Jasa UI/UX murah berkualitas" />
    <meta property="og:url" content="https://colabs.id/" />
    <meta property="og:site_name" content="COLABS INDONESIA" />
    
    <!--====== Title ======-->
    <title>COLABS INDONESIA - Ciptakan produk hebat dan berkolaborasi</title>
    
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--====== Favicon Icon ======-->
    <link rel="shortcut icon" href="<?php echo base_url().'assets/images/favicon.ico'; ?>" type="image/png">
     <link rel="shortcut icon" href="<?php echo base_url().'assets/images/favicon.ico'; ?>" type="image/x-icon">

    <!--====== Animate CSS ======-->
    <link rel="stylesheet" href="<?php echo base_url().'assets/css/animate.css'; ?>">

    <!--====== Line Icons CSS ======-->
    <link rel="stylesheet" href="<?php echo base_url().'assets/css/LineIcons.2.0.css'; ?>">

    <!--====== Bootstrap CSS ======-->
    <link rel="stylesheet" href="<?php echo base_url().'assets/css/bootstrap-4.5.0.min.css'; ?>">
    
    <!--====== Default CSS ======-->
    <link rel="stylesheet" href="<?php echo base_url().'assets/css/default.css'; ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/slick/slick.css'; ?>"/>

    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/slick/slick-theme.css'; ?>"/>
    
    <!--====== Style CSS ======-->
    <link rel="stylesheet" href="<?php echo base_url().'assets/css/style.css'; ?>">
    
</head>

<body>
    <!--[if IE]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
<![endif]-->    


<!--====== PRELOADER PART START ======-->

<div class="preloader">
    <div class="loader">
        <div class="ytp-spinner">
            <div class="ytp-spinner-container">
                <div class="ytp-spinner-rotator">
                    <div class="ytp-spinner-left">
                        <div class="ytp-spinner-circle"></div>
                    </div>
                    <div class="ytp-spinner-right">
                        <div class="ytp-spinner-circle"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--====== PRELOADER PART ENDS ======-->

<!--====== HEADER PART START ======-->

<header class="header-area">
    <div class="navbar-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <nav class="navbar navbar-expand-lg">
                        <a class="navbar-brand" href="<?php echo base_url(); ?>">
                            <img src="<?php echo base_url().'assets/images/logo.svg'; ?>" alt="Logo">
                        </a>
                       
                            
                            
                        </nav> <!-- navbar -->
                    </div>
                </div> <!-- row -->
            </div> <!-- container -->
        </div> <!-- navbar area -->
        
        <div id="home" class="header-hero bg_cover" style="background-image: url(<?php echo base_url().'assets/images/banner-bg.svg'; ?>)">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <div class="header-hero-content text-center">
                            <h3 class="header-sub-title wow fadeInUp" data-wow-duration="1.3s" data-wow-delay="0.2s">Halo, kami <b>COLABS.ID!</b></h3>
                            <h2 class="header-title wow fadeInUp" data-wow-duration="1.3s" data-wow-delay="0.5s">Ciptakan produk hebat dan berkolaborasi</h2>
                            <a href="https://api.whatsapp.com/send?phone=6285156721183&text=Halo saya ingin bertanya seputar teknologi digital&source=&data=&app_absent=" class="main-btn wow fadeInUp" data-wow-duration="1.3s" data-wow-delay="1.1s">Hubungi Kami</a>
                        </div> <!-- header hero content -->
                    </div>
                </div> <!-- row -->
              
            </div> <!-- container -->
            <div id="particles-1" class="particles"></div>
        </div> <!-- header hero -->
    </header>
    
    <!--====== HEADER PART ENDS ======-->