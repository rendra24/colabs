<?php

header('Access-Control-Allow-Origin: *');  

class Web extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('global_helper');
    }


    public function index(){

    	$this->load->view('web/header');
    	$this->load->view('web/home');
    	$this->load->view('web/footer');
    }

    public function detail($id=''){
        $data['id'] = $id;
        $this->load->view('web/header1');
        $this->load->view('web/detail',$data);
        $this->load->view('web/footer');
    }

     public function detail_proyek($id=''){
        
        $data['row'] = $this->db->get_where('contents', array('ID' => $id))->row_array();

        $this->load->view('web/header1');
        $this->load->view('web/detail_proyek',$data);
        $this->load->view('web/footer');
    }





}