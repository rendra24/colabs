<?php



header('Access-Control-Allow-Origin: *');  



class Admin extends CI_Controller {



	public function __construct()

	{

		parent::__construct();

		$this->load->helper('global_helper');



    if(!$this->session->userdata('id_user')){

      redirect(base_url().'auth');

    }

	}





	public function index(){



		$head['menu'] = 'home';



		$this->load->view('admin/header',$head);

		$this->load->view('admin/home');

		$this->load->view('admin/footer');

	}



	public function menu(){

		$head['menu'] = 'menu';

		$data['menus'] = $this->db->get('menu')->result_array();

		$this->load->view('admin/header',$head);

		$this->load->view('admin/menu', $data);

		$this->load->view('admin/footer');

	}



    public function kontak(){

    $head['menu'] = 'kontak';

    $data['kontak'] = $this->db->get('kontak')->result_array();

    $this->load->view('admin/header',$head);

    $this->load->view('admin/kontak', $data);

    $this->load->view('admin/footer');

  }



	public function media(){

		$head['menu'] = 'media';

		$data['media'] = $this->db->get('media')->result_array();

		$this->load->view('admin/header',$head);

		$this->load->view('admin/media', $data);

		$this->load->view('admin/footer');

	}



	public function setting(){

		$head['menu'] = 'setting';

		$data['row'] = $this->db->get_where('setting', array('id' => 1))->row_array();

    $data['fitur'] = $this->db->get('fitur')->result_array();

		$this->load->view('admin/header',$head);

		$this->load->view('admin/setting', $data);

		$this->load->view('admin/footer');

	}



	public function konten($id_menu=''){

		$head['menu'] = 'menu';

		$data['page'] = $this->db->get_where('menu', array('id' => $id_menu))->row_array();

		$data['konten'] = $this->db->get_where('contents', array('ID_MENU' => $id_menu))->result_array();

		$this->load->view('admin/header',$head);

		$this->load->view('admin/konten', $data);

		$this->load->view('admin/footer');

	}



	public function produk(){

		$head['menu'] = 'produk';

		$data['produk'] = $this->db->get('produk')->result_array();

		$this->load->view('admin/header',$head);

		$this->load->view('admin/produk', $data);

		$this->load->view('admin/footer');

	}



	public function form_menu($id=''){

		$head['menu'] = 'menu';



		if($id != ''){

			$data['row'] = $this->db->get_where('menu', array('id' => $id))->row_array();

		}else{

			$data = '';

		}

		

		$this->load->view('admin/header',$head);

		$this->load->view('admin/form_menu',$data);

		$this->load->view('admin/footer');

	}



		public function form_konten($id_menu='', $id=''){

		$head['menu'] = 'menu';



		$data['page'] = $this->db->get_where('menu', array('id' => $id_menu))->row_array();

		if($id != ''){

			$data['row'] = $this->db->get_where('contents', array('ID' => $id))->row_array();

		}



		



		



		

		$this->load->view('admin/header',$head);

		$this->load->view('admin/form_konten',$data);

		$this->load->view('admin/footer');

	}





		public function form_produk($id=''){

		$head['menu'] = 'menu';

		

		if($id != ''){

			$data['row'] = $this->db->get_where('produk', array('id' => $id))->row_array();

		}else{

			$data='';

		}



		

		

		$this->load->view('admin/header',$head);

		$this->load->view('admin/form_produk',$data);

		$this->load->view('admin/footer');

	}



	public function simpan_menu(){

		if($post = $this->input->post()){



			$data['nama_menu'] = $post['nama_menu'];

			$data['keterangan'] = $post['keterangan'];

			$data['icon'] = $post['icon'];

      $data['flag'] = $post['flag'];



			if($post['id'] != ''){

				if($this->db->update('menu',$data, array('id' => $post['id']))){

					echo 1;

				}

			}else{

				if($this->db->insert('menu',$data)){

					echo 1;

				}

			}

			

		}

	}



  function simpan_setting(){

    if($post = $this->input->post()){



    $filename=$_FILES['userfile']['name'];

    $filename = str_replace(' ','-',$filename);

    $filename = strtolower($filename);



    if($_FILES['userfile']['name'] != '') {

      $config['upload_path'] = "./assets_foto/";

      $config['file_name'] = $filename;

      $config['overwrite'] = TRUE;

      $config["allowed_types"] = 'svg|jpg|jpeg|png|gif|ico';

      $config["max_size"] = 1920;

      $config["max_width"] = 1920;

      $config["max_height"] = 1280;

      $this->load->library('upload', $config);



      if(!$this->upload->do_upload()) {

       

       echo $this->upload->display_errors();



      } else {

        

        $data['logo'] = $filename;

      }

    }





      $data['nama_perusahaan'] = $post['nama_perusahaan'];

      $data['email'] = $post['email'];

      $data['no_tlp'] = $post['no_tlp'];

      $data['alamat'] = $post['alamat'];



      $this->db->update('setting',$data, array('id' => $post['id']));



    $fitur = $this->db->get('fitur')->result_array();

    foreach ($fitur as $row) {

      $id = $row['id'];



      if(isset($post['fitur'.$id])){

        $add['flag'] = 1;

        $this->db->update('fitur',$add, array('id' => $id));

      }else{

         $add['flag'] = 0;

        $this->db->update('fitur',$add, array('id' => $id));

      }

      

    }

    echo 1;

    }

  }



	function simpan_konten($flag=0)

{

  $filename = '';

  if($post = $this->input->post())

  {



    $filename=$_FILES['userfile']['name'];

    $filename = str_replace(' ','-',$filename);

    $filename = strtolower($filename);



    if($_FILES['userfile']['name'] != '') {

      $config['upload_path'] = "./assets_foto/";

      $config['file_name'] = $filename;

      $config['overwrite'] = TRUE;

      $config["allowed_types"] = 'svg|jpg|jpeg|png|gif';

      $config["max_size"] = 1920;

      $config["max_width"] = 1920;

      $config["max_height"] = 1280;

      $this->load->library('upload', $config);



      if(!$this->upload->do_upload()) {

       

        $json['success'] = false;

        $json['message'] = $this->upload->display_errors();



        echo json_encode($json);

        exit;



      } else {

        

        $data['IMAGE'] = $filename;

      }

    }





    $title = $this->input->post('META_TITLE');

    $hasil = url_slug($title);



  	$data['META_TITLE'] = $post['META_TITLE'];

  	$data['URL_SLUG']= $hasil;

  	$data['META_KEYWORD'] = $post['META_KEYWORD'];

  	$data['META_DESC'] = $post['META_DESC'];

  	$data['ID_MENU'] = $post['ID_MENU'];

  	$data['CREATED_DATE'] = date('Y-m-d H:i:s');



  	if($post['ID'] != ''){

  	$data['EDITED_DATE'] = date('Y-m-d H:i:s');

  	$data['FLAG'] = $post['FLAG'];

  		if($this->db->update('contents',$data, array('ID' => $post['ID']))){

  			$json['id_menu'] = $post['ID_MENU'];

  			$json['success'] = true;



  			

  		}

  	}else{

  		if($this->db->insert('contents',$data)){

  			$json['id_menu'] = $post['ID_MENU'];

  			$json['success'] = true;



  			

  		}

  	}



  	echo json_encode($json);



    





  }

}



function simpan_produk($flag=0)

{

  $filename = '';

  if($post = $this->input->post())

  {



    $filename=$_FILES['userfile']['name'];

    $filename = str_replace(' ','-',$filename);

    $filename = strtolower($filename);



    if($_FILES['userfile']['name'] != '') {

      $config['upload_path'] = "./assets_foto/produk/";

      $config['file_name'] = $filename;

      $config['overwrite'] = TRUE;

      $config["allowed_types"] = 'svg|jpg|jpeg|png|gif';

      $config["max_size"] = 1920;

      $config["max_width"] = 1920;

      $config["max_height"] = 1280;

      $this->load->library('upload', $config);



      if(!$this->upload->do_upload()) {

       

        $json['success'] = false;

        $json['message'] = $this->upload->display_errors();



        echo json_encode($json);

        exit;



      } else {

        

        $data['gambar'] = $filename;

      }

    }



  	$data['nama_produk'] = $post['nama_produk'];

  	$data['harga'] = $post['harga'];

  	$data['keterangan'] = $post['keterangan'];

  	$data['created_date'] = date('Y-m-d H:i:s');



  	if($post['id'] != ''){

  	$data['edited_date'] = date('Y-m-d H:i:s');

  	$data['flag'] = $post['flag'];

  		if($this->db->update('produk',$data, array('id' => $post['id']))){

  			

  			$json['success'] = true;



  			

  		}

  	}else{

  		if($this->db->insert('produk',$data)){

  			

  			$json['success'] = true;



  			

  		}

  	}



  	echo json_encode($json);



    





  }

}





public function simpan_media(){

	$filename = '';

  if($post = $this->input->post())

  {

  	$filename=$_FILES['userfile']['name'];

    $filename = str_replace(' ','-',$filename);

    $filename = strtolower($filename);



    if($_FILES['userfile']['name'] != '') {

      $config['upload_path'] = "./media/";

      $config['file_name'] = $filename;

      $config['overwrite'] = TRUE;

      $config["allowed_types"] = 'svg|jpg|jpeg|png|gif';

      $config["max_size"] = 1920;

      $config["max_width"] = 1920;

      $config["max_height"] = 1280;

      $this->load->library('upload', $config);



      if(!$this->upload->do_upload()) {

       

        $json['success'] = false;

        $json['message'] = $this->upload->display_errors();



        echo json_encode($json);

        exit;



      } else {

        

        $data['media'] = $filename;

      }

    }



    $data['title'] = $post['title'];

    $data['created_date'] = date('Y-m-d H:i:s');





    if($this->db->insert('media',$data)){

    	$json['success'] = true;

    	$json['new_image'] = base_url().'media/'.$filename;



    	echo json_encode($json);

    }





  }

}

















}