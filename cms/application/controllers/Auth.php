<?php

header('Access-Control-Allow-Origin: *');  

class Auth extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}


	public function index(){

	
		$this->load->view('masuk/masuk');
	}



	public function do_regis(){


   
    if($post = $this->input->post()){

        $cek = $this->db->get_where('admin', array('email' => $post['email']));

        if($cek->num_rows() > 0){
            echo "Email sudah di gunakan";
        }else{

            $data['email'] = $post['email'];
            $data['nama'] = $post['nama'];
            $data['password'] = password_hash($post['password'], PASSWORD_BCRYPT);
            $data['created_date'] = date('Y-m-d H:i:s');


            if($this->db->insert('admin', $data)){
                echo 1;
            } 
        }


    }
}

	public function do_login(){

        if($post = $this->input->post()){

            $email = $post['email'];
            $userpass = $post['password'];



            $cek = $this->db->get_where('admin',  array('email' => $email));

            if($cek->num_rows() > 0){
                $row = $cek->row_array();

                if (password_verify($userpass, $row['password'])) {
                 $newdata = array(
                    'id_user'   => $row['id'],
                    'nama'      => $row['nama'],
                    'email'     => $row['email'],
                    'logged_in' => TRUE
                );

                 $this->session->set_userdata($newdata);
                 echo 1;
             }

         }else{
            echo "password atau email salah";
        }
    }else{
        echo "Error Server";
    }
}

public function keluar(){
         session_destroy();

         redirect(base_url().'auth');
}

}

?>