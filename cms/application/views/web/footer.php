
<!--====== Jquery js ======-->
<script src="<?php echo base_url().'assets/js/vendor/jquery-3.5.1-min.js'; ?>"></script>

<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/slick/slick.min.js'; ?>"></script>

<script type="text/javascript">

    //$('.slick-carosel').slick();

    $('.slick-carosel').slick({
        dots: true,
      infinite: true,
      slidesToShow: 5,
      slidesToScroll: 5,
      autoplay: true,
      autoplaySpeed: 5000
  });

</script>
<script src="<?php echo base_url().'assets/js/vendor/modernizr-3.7.1.min.js'; ?>"></script>

<!--====== Bootstrap js ======-->
<script src="<?php echo base_url().'assets/js/popper.min.js'; ?>"></script>
<script src="<?php echo base_url().'assets/js/bootstrap-4.5.0.min.js'; ?>"></script>

<!--====== Plugins js ======-->
<script src="<?php echo base_url().'assets/js/plugins.js'; ?>"></script>

<!--====== Counter Up js ======-->
<script src="<?php echo base_url().'assets/js/waypoints.min.js'; ?>"></script>
<script src="<?php echo base_url().'assets/js/jquery.counterup.min.js'; ?>"></script>



<!--====== Scrolling Nav js ======-->
<script src="<?php echo base_url().'assets/js/jquery.easing.min.js'; ?>"></script>
<script src="<?php echo base_url().'assets/js/scrolling-nav.js'; ?>"></script>

<!--====== wow js ======-->
<script src="<?php echo base_url().'assets/js/wow.min.js'; ?>"></script>

<!--====== Particles js ======-->
<script src="<?php echo base_url().'assets/js/particles.min.js'; ?>"></script>

<!--====== Main js ======-->
<script src="<?php echo base_url().'assets/js/main.js'; ?>"></script>

</body>

</html>