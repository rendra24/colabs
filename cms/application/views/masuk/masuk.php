
<!DOCTYPE html>
<html lang="en">
<!--begin::Head-->
<head>
	
	<meta charset="utf-8" />
	<title>Metronic | Login Page 1</title>
	<meta name="description" content="Login page example" />
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
	
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
	
	<link href="https://preview.keenthemes.com/metronic/theme/html/demo6/dist/assets/css/pages/login/classic/login-1.css?v=7.0.8" rel="stylesheet" type="text/css" />
	
	<link href="https://preview.keenthemes.com/metronic/theme/html/demo6/dist/assets/css/style.bundle.css?v=7.0.8" rel="stylesheet" type="text/css" />
	
	<link rel="shortcut icon" href="/metronic/theme/html/demo6/dist/assets/media/logos/favicon.ico" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" type="text/javascript"></script>


</head>
<body id="kt_body" class="header-fixed header-mobile-fixed subheader-enabled sidebar-enabled page-loading">
	<!-- Google Tag Manager (noscript) -->
	<noscript>
		<iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5FS8GGP" height="0" width="0" style="display:none;visibility:hidden"></iframe>
	</noscript>
	<!-- End Google Tag Manager (noscript) -->
	<!--begin::Main-->
	<div class="d-flex flex-column flex-root">
		<!--begin::Login-->
		<div class="login login-1 login-signin-on d-flex flex-column flex-lg-row flex-column-fluid bg-white" id="kt_login">
			<!--begin::Aside-->
			<div class="login-aside d-flex flex-row-auto bgi-size-cover bgi-no-repeat p-10 p-lg-10" style="background-image: url(<?php echo base_url().'assets/media/bg/bg-4.jpg';?>);">
				<!--begin: Aside Container-->
				<div class="d-flex flex-row-fluid flex-column justify-content-between">
					<!--begin: Aside header-->
					<a href="#" class="flex-column-auto mt-5 pb-lg-0 pb-10">
						<img src="/metronic/theme/html/demo6/dist/assets/media/logos/logo-letter-1.png" class="max-h-70px" alt="" />
					</a>
					<!--end: Aside header-->
					<!--begin: Aside content-->
					<div class="flex-column-fluid d-flex flex-column justify-content-center">
						<h3 class="font-size-h1 mb-5 text-white">Welcome to COLABS!</h3>
						<p class="font-weight-lighter text-white opacity-80">Admin panels</p>
					</div>
					<!--end: Aside content-->
					<!--begin: Aside footer for desktop-->
					<div class="d-none flex-column-auto d-lg-flex justify-content-between mt-10">
						<div class="opacity-70 font-weight-bold text-white">© 2020 Metronic</div>
						<div class="d-flex">
							<a href="#" class="text-white">Privacy</a>
							<a href="#" class="text-white ml-10">Legal</a>
							<a href="#" class="text-white ml-10">Contact</a>
						</div>
					</div>
					<!--end: Aside footer for desktop-->
				</div>
				<!--end: Aside Container-->
			</div>
			<!--begin::Aside-->
			<!--begin::Content-->
			<div class="d-flex flex-column flex-row-fluid position-relative p-7 overflow-hidden">
				
				<!--begin::Content body-->
				<div class="d-flex flex-column-fluid flex-center mt-30 mt-lg-0">
					<!--begin::Signin-->
					<div class="login-form login-signin">
						<div class="text-center mb-10 mb-lg-20">
							<h3 class="font-size-h1">Sign In</h3>
							<p class="text-muted font-weight-bold">Enter your username and password</p>
						</div>
						<!--begin::Form-->
						<form class="formlogin">
							<div class="form-group">
								<input class="form-control form-control-solid h-auto py-5 px-6" type="text" placeholder="Email" name="email" autocomplete="off" />
							</div>
							<div class="form-group">
								<input class="form-control form-control-solid h-auto py-5 px-6" type="password" placeholder="Password" name="password" autocomplete="off" />
							</div>
							<!--begin::Action-->
							<div class="form-group d-flex flex-wrap justify-content-between align-items-center">
								<a href="javascript:;" class="text-dark-50 text-hover-primary my-3 mr-2" id="kt_login_forgot">Forgot Password ?</a>
								<button type="submit" class="btn btn-primary font-weight-bold px-9 py-4 my-3">Sign In</button>
							</div>
							<!--end::Action-->
						</form>
						<!--end::Form-->
					</div>
					<!--end::Signin-->

					
				</div>
				<!--end::Content body-->
				<!--begin::Content footer for mobile-->
				<div class="d-flex d-lg-none flex-column-auto flex-column flex-sm-row justify-content-between align-items-center mt-5 p-5">
					<div class="text-dark-50 font-weight-bold order-2 order-sm-1 my-2">© 2020 Metronic</div>
					<div class="d-flex order-1 order-sm-2 my-2">
						<a href="#" class="text-dark-75 text-hover-primary">Privacy</a>
						<a href="#" class="text-dark-75 text-hover-primary ml-4">Legal</a>
						<a href="#" class="text-dark-75 text-hover-primary ml-4">Contact</a>
					</div>
				</div>
				<!--end::Content footer for mobile-->
			</div>
			<!--end::Content-->
		</div>
		<!--end::Login-->
	</div>
	
	<script type="text/javascript">
		


		$(".formlogin").submit(function(event){
			event.preventDefault();
			
			var formData = new FormData(this);

			$.ajax({
				type: "POST",
				url: "<?php echo base_url().'auth/do_login'; ?>",
				data:formData,
				processData:false,
				contentType:false,
				cache:false,
				async:false,
				success: function(data) {

					if(data == 1)
					{
						window.location = '<?php echo base_url().'admin'; ?>';
					}else{
						swal.fire({
							icon: 'error',
							title: 'Gagal',
							text: data.message
						});
					}
				}
			});

		});
	</script>
</body>
<!--end::Body-->
</html>