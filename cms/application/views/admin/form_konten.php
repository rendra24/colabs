<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>
    


<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
  <div class="row">
    <div class="col-lg-12">
      <div class="card card-custom">
       <div class="card-header">
        <h3 class="card-title">
         <?php 
         echo $page['nama_menu']; ?>
       </h3>
       <div class="card-toolbar">
         <div class="example-tools justify-content-center">
          <span class="example-toggle" data-toggle="tooltip" title="View code"></span>
          <span class="example-copy" data-toggle="tooltip" title="Copy code"></span>
        </div>
      </div>
    </div>
    <!--begin::Form-->

    <?php 
    if($this->uri->segment(4)){
      $judul = $row['META_TITLE'];
      $keterangan = $row['META_DESC'];
      $keyword = $row['META_KEYWORD'];
      $id = $row['ID'];
      $id_menu = $row['ID_MENU'];
      $flag = $row['FLAG'];
    }else{
      $judul = '';
      $keterangan = '';
      $keyword = '';
      $id = '';
      $id_menu = $this->uri->segment(3);
      $flag = '';
    }
    ?>
    <form class="formkonten">
      <div class="card-body">
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label>Judul<span class="text-danger">*</span></label>
              <input type="text" name="META_TITLE" class="form-control" value="<?php echo $judul; ?>" />
              <!-- <span class="form-text text-muted">Nama menu tidak boleh sama</span> -->
            </div>

            <div class="form-group">
              <label>Keyword<span class="text-danger">*</span></label>
              <input type="text" name="META_KEYWORD" class="form-control" value="<?php echo $keyword; ?>" />
            </div>
          </div>
          <div class="col-md-6">


           <div class="form-group">
            <label>Gambar <span class="text-danger">*</span></label>
            <input type="file" name="userfile" class="form-control" />
          </div>

          <?php if($id != ''){ ?>
           <div class="form-group">
            <label>Status <span class="text-danger">*</span></label>
            <select name="FLAG" class="form-control">
              <option>-- PILIH STATUS --</option>
              <option value="1" <?php if($flag == 1){ echo "selected"; } ?> >Aktif</option>
              <option value="0" <?php if($flag == 0){ echo "selected"; } ?> >Non AKtif</option>
            </select>
          </div>
        <?php } ?>
        </div>

        <div class="col-md-12">
          <div class="form-group mb-1">
            <label for="exampleTextarea">Keterangan <span class="text-danger">*</span></label>
            <div id="summernote"><?php echo $keterangan; ?></div>
          </div>
        </div>
      </div>




    </div>
    <input type="hidden" name="ID" value="<?php echo $id; ?>">
    <input type="hidden" name="ID_MENU" value="<?php echo $id_menu; ?>">
    <div class="card-footer">
     <button type="submit" class="btn btn-primary mr-2">Simpan</button>
     <button type="reset" class="btn btn-secondary">Batal</button>
   </div>
 </form>
 <!--end::Form-->
</div>
</div>
</div>
</div>


<script type="text/javascript">


jQuery(document).ready(function() {

   
    $('#summernote').summernote({
        placeholder: 'Keterangan disini..',
        tabsize: 2,
        height: 100
      });

   $(".formkonten").submit(function(event){
    event.preventDefault();
    
     var desc = $('#summernote').summernote('code');

    var formData = new FormData(this);

    formData.append('META_DESC',desc);

    $.ajax({
      type: "POST",
      url: "<?php echo base_url().'admin/simpan_konten'; ?>",
      data:formData,
      dataType: 'json',
      processData:false,
      contentType:false,
      cache:false,
      async:false,
      success: function(data) {

        if(data.success == true)
        {
          window.location = '<?php echo base_url().'admin/konten/'; ?>'+data.id_menu;
        }else{
           swal.fire({
  icon: 'error',
  title: 'Gagal',
  text: data.message
});
        }
      }
    });

  });

});
</script>