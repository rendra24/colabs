<script src="<?php echo base_url().'assets/js/ckeditor-classic.bundle.js';?>"></script>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
  <div class="row">
    <div class="col-lg-12">
      <div class="card card-custom">
       <div class="card-header">
        <h3 class="card-title">
         Tambahakan Produk
       </h3>
       <div class="card-toolbar">
         <div class="example-tools justify-content-center">
          <span class="example-toggle" data-toggle="tooltip" title="View code"></span>
          <span class="example-copy" data-toggle="tooltip" title="Copy code"></span>
        </div>
      </div>
    </div>
    <!--begin::Form-->

    <?php 
    if($this->uri->segment(3)){
      $nama_produk = $row['nama_produk'];
      $harga = $row['harga'];
      $keterangan = $row['keterangan'];
      $id = $row['id'];
      $flag = $row['flag'];
    }else{
      $nama_produk = '';
      $keterangan = '';
      $harga = '';
      $id = '';
      $flag = '';
    }
    ?>
    <form class="formkonten">
      <div class="card-body">
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label>Nama Produk<span class="text-danger">*</span></label>
              <input type="text" name="nama_produk" class="form-control" value="<?php echo $nama_produk; ?>" />
              <!-- <span class="form-text text-muted">Nama menu tidak boleh sama</span> -->
            </div>

            <div class="form-group">
              <label>Harga<span class="text-danger">*</span></label>
              <input type="text" name="harga" class="form-control" value="<?php echo $harga; ?>" />
            </div>
          </div>
          <div class="col-md-6">


           <div class="form-group">
            <label>Gambar <span class="text-danger">*</span></label>
            <input type="file" name="userfile" class="form-control" />
          </div>

          <?php if($id != ''){ ?>
           <div class="form-group">
            <label>Status <span class="text-danger">*</span></label>
            <select name="flag" class="form-control">
              <option>-- PILIH STATUS --</option>
              <option value="1" <?php if($flag == 1){ echo "selected"; } ?> >Aktif</option>
              <option value="0" <?php if($flag == 0){ echo "selected"; } ?> >Non AKtif</option>
            </select>
          </div>
        <?php } ?>
        </div>

        <div class="col-md-12">
          <div class="form-group mb-1">
            <label for="exampleTextarea">Keterangan <span class="text-danger">*</span></label>
            <textarea  id="kt-ckeditor-1"><?php echo $keterangan; ?></textarea>
          </div>
        </div>
      </div>




    </div>
    <input type="hidden" name="id" value="<?php echo $id; ?>">
    <div class="card-footer">
     <button type="submit" class="btn btn-primary mr-2">Simpan</button>
     <button type="reset" class="btn btn-secondary">Batal</button>
   </div>
 </form>
 <!--end::Form-->
</div>
</div>
</div>
</div>


<script type="text/javascript">


  var metadesc;
  var KTCkeditor = function () {
    // Private functions
    var demos = function () {
      ClassicEditor
      .create( document.querySelector( '#kt-ckeditor-1' ) )
      .then( editor => {
        metadesc = editor;
      } )
      .catch( error => {
        console.error( error );
      } );
    }

    return {
        // public functions
        init: function() {
          demos();
        }
      };
    }();

// Initialization
jQuery(document).ready(function() {
  KTCkeditor.init();

   $(".formkonten").submit(function(event){
    event.preventDefault();
    
    var desc = metadesc.getData();

    var formData = new FormData(this);

    formData.append('keterangan',desc);

    $.ajax({
      type: "POST",
      url: "<?php echo base_url().'admin/simpan_produk'; ?>",
      data:formData,
      dataType: 'json',
      processData:false,
      contentType:false,
      cache:false,
      async:false,
      success: function(data) {

        if(data.success == true)
        {
          window.location = '<?php echo base_url().'admin/produk/'; ?>';
        }else{
           swal.fire({
  icon: 'error',
  title: 'Gagal',
  text: data.message
});
        }
      }
    });

  });

});
</script>