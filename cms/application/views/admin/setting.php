
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
  <div class="row">
    <div class="col-lg-12">
      <div class="card card-custom">
       <div class="card-header">
        <h3 class="card-title">
        Pengaturan
       </h3>
       <div class="card-toolbar">
         <div class="example-tools justify-content-center">
          <span class="example-toggle" data-toggle="tooltip" title="View code"></span>
          <span class="example-copy" data-toggle="tooltip" title="Copy code"></span>
        </div>
      </div>
    </div>
    <!--begin::Form-->

    <?php 
      $nama_perusahaan = $row['nama_perusahaan'];
      $email = $row['email'];
      $no_tlp = $row['no_tlp'];
      $id = $row['id'];
      $alamat = $row['alamat'];
    ?>
    <form class="formkonten">
      <div class="card-body">
        <div class="row">
          <div class="col-md-8">
            <div class="form-group">
              <label>Nama Perusahaan<span class="text-danger">*</span></label>
              <input type="text" name="nama_perusahaan" class="form-control" value="<?php echo $nama_perusahaan; ?>" />
              <!-- <span class="form-text text-muted">Nama menu tidak boleh sama</span> -->
            </div>

            <div class="form-group">
              <label>Email<span class="text-danger">*</span></label>
              <input type="text" name="email" class="form-control" value="<?php echo $email; ?>" />
            </div>

              <div class="form-group">
            <label>Nomor Telephone <span class="text-danger">*</span></label>
            <input type="number" name="no_tlp" class="form-control"value="<?php echo $no_tlp; ?>" />
          </div>

          <div class="form-group">
            <label>Alamat<span class="text-danger">*</span></label>
            <textarea name="alamat" class="form-control"><?php echo $alamat; ?></textarea>
          </div>

          <div class="form-group">
            <label>Logo <span class="text-danger">*</span></label>
            <input type="file" name="userfile" class="form-control" />
          </div>

          </div>
         <div class="col-md-4">
          <div class="kt-portlet kt-portlet--tabs kt-portlet--height-fluid">
                    <div class="kt-portlet__head">
                      <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                          Fitur Website
                        </h3>
                      </div>
                      <!-- <div class="kt-portlet__head-toolbar" style="padding-top: 15px;padding-bottom: 15px;">
                        <a href="<?php echo base_url().'admin/form_konten/'.$this->uri->segment(3); ?>" class="btn btn-label-brand btn-bold btn-sm">Simpan</a>
                      </div> -->

                    </div>
                    <div class="kt-portlet__body">

                  

                          <div class="kt-widget11">
                            <div class="table-responsive">
                              <table class="table">
                                <thead>
                                  <tr>
                                    <td style="width:10%">#</td>
                                    <td style="width:90%">Fitur</td>
                                  </tr>
                                </thead>
                                <tbody>
                                  <?php foreach ($fitur as $fitur) {
                                    ?>

                                    <tr>
                                   <td>
                                      <label class="kt-checkbox kt-checkbox--single">
                                        <input type="checkbox" name="fitur<?php echo $fitur['id']; ?>" <?php if($fitur['flag'] == '1'){
                                          echo "checked";
                                        } ?> value="1"><span></span>
                                      </label>
                                    </td>
                                    <td>
                                      <a href="#" class="kt-widget11__title"><?php echo $fitur['fitur']; ?></a>
                                    </td>
                                  </tr>

                                    <?php } ?>
                                 
                                </tbody>
                              </table>
                            </div>
                          </div>


                    </div>
                  </div>
          </div>
      
      </div>




    </div>
    <input type="hidden" name="id" value="<?php echo $id; ?>">
    <div class="card-footer">
     <button type="submit" class="btn btn-primary mr-2">Simpan</button>
   </div>
 </form>
 <!--end::Form-->
</div>
</div>
</div>
</div>


<script type="text/javascript">


jQuery(document).ready(function() {
  

   $(".formkonten").submit(function(event){
    event.preventDefault();
    var formData = new FormData(this);

    $.ajax({
      type: "POST",
      url: "<?php echo base_url().'admin/simpan_setting'; ?>",
      data:formData,
      processData:false,
      contentType:false,
      cache:false,
      async:false,
      success: function(data) {

        if(data == 1)
        {
            swal.fire({
            icon: 'success',
            title: 'Berhasil',
            text: 'Data berhasil di simpan'
          });

            location.reload();
        }else{
           swal.fire({
            icon: 'error',
            title: 'Gagal',
            text: data
          });
        }
      }
    });

  });

});
</script>