<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
  <div class="row">
    <div class="col-lg-6">
<div class="card card-custom">
 <div class="card-header">
  <h3 class="card-title">
   Menu
  </h3>
  <div class="card-toolbar">
   <div class="example-tools justify-content-center">
    <span class="example-toggle" data-toggle="tooltip" title="View code"></span>
    <span class="example-copy" data-toggle="tooltip" title="Copy code"></span>
   </div>
  </div>
 </div>
 <!--begin::Form-->

 <?php 
  if($this->uri->segment(3)){
    $nama_menu = $row['nama_menu'];
    $keterangan = $row['keterangan'];
    $icon = $row['icon'];
    $id = $row['id'];
    $flag = $row['flag'];
  }else{
    $nama_menu = '';
    $keterangan = '';
    $icon = '';
    $id = '';
    $flag = '';
  }
  ?>
 <form class="formmenu">
  <div class="card-body">
   <div class="form-group">
    <label>Nama Menu <span class="text-danger">*</span></label>
    <input type="text" name="nama_menu" class="form-control" value="<?php echo $nama_menu; ?>" />
    <span class="form-text text-muted">Nama menu tidak boleh sama</span>
   </div>
   <div class="form-group">
    <label>Icon <span class="text-danger">*</span></label>
    <input type="text" name="icon" class="form-control" value="<?php echo $icon; ?>" />
   </div>
   <div class="form-group mb-1">
    <label for="exampleTextarea">Keterangan <span class="text-danger">*</span></label>
    <textarea class="form-control" name="keterangan" rows="3"><?php echo $keterangan; ?></textarea>
   </div>
    <?php if($id != ''){ ?>
           <div class="form-group">
            <label>Status <span class="text-danger">*</span></label>
            <select name="flag" class="form-control">
              <option>-- PILIH STATUS --</option>
              <option value="1" <?php if($flag == 1){ echo "selected"; } ?> >Aktif</option>
              <option value="0" <?php if($flag == 0){ echo "selected"; } ?> >Non AKtif</option>
            </select>
          </div>
        <?php } ?>
  </div>
  <input type="hidden" name="id" value="<?php echo $id; ?>">
  <div class="card-footer">
   <button type="submit" class="btn btn-primary mr-2">Simpan Menu</button>
   <button type="reset" class="btn btn-secondary">Batal</button>
  </div>
 </form>
 <!--end::Form-->
</div>
</div>
</div>
</div>


<script type="text/javascript">
  $(".formmenu").submit(function(event){
    event.preventDefault();
    
   var formData = new FormData(this);

    $.ajax({
      type: "POST",
      url: "<?php echo base_url().'admin/simpan_menu'; ?>",
      data:formData,
     processData:false,
     contentType:false,
     cache:false,
     async:false,
      success: function(data) {

        if(data == 1)
        {
          window.location = '<?php echo base_url().'admin/menu'; ?>';
        }
      }
    });

  });
</script>