<!DOCTYPE html>

<html lang="en">

	<!-- begin::Head -->
	<head>

		<!--begin::Base Path (base relative path for assets of this page) -->
		<base href="../">

		<!--end::Base Path -->
		<meta charset="utf-8" />
		<title>COLABS | Dashboard</title>
		<meta name="description" content="Latest updates and statistic charts">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!--begin::Fonts -->
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
			WebFont.load({
				google: {
					"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
				},
				active: function() {
					sessionStorage.fonts = true;
				}
			});
		</script>

		<!--end::Fonts -->

		<!--begin::Page Vendors Styles(used by this page) -->
		<link href="<?php echo base_url().'assets/vendors/custom/fullcalendar/fullcalendar.bundle.css';?>" rel="stylesheet" type="text/css" />

		<!--end::Page Vendors Styles -->

		<!--begin:: Global Mandatory Vendors -->
		<link href="<?php echo base_url().'assets/vendors/general/perfect-scrollbar/css/perfect-scrollbar.css';?>" rel="stylesheet" type="text/css" />

		<!--end:: Global Mandatory Vendors -->

		<!--begin:: Global Optional Vendors -->
		<link href="<?php echo base_url().'assets/vendors/general/tether/dist/css/tether.css';?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url().'assets/vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css';?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url().'assets/vendors/general/bootstrap-datetime-picker/css/bootstrap-datetimepicker.css';?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url().'assets/vendors/general/bootstrap-timepicker/css/bootstrap-timepicker.css';?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url().'assets/vendors/general/bootstrap-daterangepicker/daterangepicker.css';?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url().'assets/vendors/general/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.css';?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url().'assets/vendors/general/bootstrap-select/dist/css/bootstrap-select.css';?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url().'assets/vendors/general/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css';?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url().'assets/vendors/general/select2/dist/css/select2.css';?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url().'assets/vendors/general/ion-rangeslider/css/ion.rangeSlider.css';?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url().'assets/vendors/general/nouislider/distribute/nouislider.css';?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url().'assets/vendors/general/owl.carousel/dist/assets/owl.carousel.css';?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url().'assets/vendors/general/owl.carousel/dist/assets/owl.theme.default.css';?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url().'assets/vendors/general/dropzone/dist/dropzone.css';?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url().'assets/vendors/general/summernote/dist/summernote.css';?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url().'assets/vendors/general/bootstrap-markdown/css/bootstrap-markdown.min.css';?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url().'assets/vendors/general/animate.css/animate.css';?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url().'assets/vendors/general/toastr/build/toastr.css';?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url().'assets/vendors/general/morris.js/morris.css';?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url().'assets/vendors/general/sweetalert2/dist/sweetalert2.css';?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url().'assets/vendors/general/socicon/css/socicon.css';?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url().'assets/vendors/custom/vendors/line-awesome/css/line-awesome.css';?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url().'assets/vendors/custom/vendors/flaticon/flaticon.css';?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url().'assets/vendors/custom/vendors/flaticon2/flaticon.css';?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url().'assets/vendors/general/@fortawesome/fontawesome-free/css/all.min.css';?>" rel="stylesheet" type="text/css" />

		<!--end:: Global Optional Vendors -->

		<!--begin::Global Theme Styles(used by all pages) -->
		<link href="<?php echo base_url().'assets/css/demo6/style.bundle.css';?>" rel="stylesheet" type="text/css" />

		<!--end::Global Theme Styles -->

		<!--begin::Layout Skins(used by all pages) -->

		<!--end::Layout Skins -->
		<link rel="shortcut icon" href="<?php echo base_url().'favicon.ico';?>" />

		<script src="<?php echo base_url().'assets/vendors/general/jquery/dist/jquery.js';?>" type="text/javascript"></script>


		<script src="<?php echo base_url().'assets/vendors/general/popper.js/dist/umd/popper.js';?>" type="text/javascript"></script>
		<script src="<?php echo base_url().'assets/vendors/general/bootstrap/dist/js/bootstrap.min.js';?>" type="text/javascript"></script>

	</head>

	<!-- end::Head -->

	<!-- begin::Body -->
	<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-aside--minimize kt-page--loading">

		<!-- begin:: Page -->

		<!-- begin:: Header Mobile -->
		<div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
			<div class="kt-header-mobile__logo">
				<a href="<?php echo base_url().'admin'; ?>">
					<img alt="Logo" src="<?php echo base_url().'assets/media/logos/logo-6-sm.png';?>" />
				</a>
			</div>
			<div class="kt-header-mobile__toolbar">
				<div class="kt-header-mobile__toolbar-toggler kt-header-mobile__toolbar-toggler--left" id="kt_aside_mobile_toggler"><span></span></div>
				<div class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></div>
				<div class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></div>
			</div>
		</div>

		<!-- end:: Header Mobile -->
		<div class="kt-grid kt-grid--hor kt-grid--root">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

				<!-- begin:: Aside -->
				<button class="kt-aside-close " id="kt_aside_close_btn"><i class="la la-close"></i></button>
				<div class="kt-aside  kt-aside--fixed  kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop" id="kt_aside">

					<!-- begin:: Aside Menu -->
					<div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
						<div id="kt_aside_menu" class="kt-aside-menu " data-ktmenu-vertical="1" data-ktmenu-scroll="1" data-ktmenu-dropdown-timeout="500">
							<ul class="kt-menu__nav ">
								<li class="kt-menu__item  <?php if($menu == 'home'){ echo "kt-menu__item--active"; } ?>" aria-haspopup="true"><a href="<?php echo base_url().'admin'; ?>" class="kt-menu__link "><i class="kt-menu__link-icon flaticon2-protection"></i><span class="kt-menu__link-text">Dashboard</span></a></li>

								<li class="kt-menu__item <?php if($menu == 'menu'){ echo "kt-menu__item--active"; } ?>" aria-haspopup="true"><a href="<?php echo base_url().'admin/menu'; ?>" class="kt-menu__link "><i class="kt-menu__link-icon flaticon-web"></i><span class="kt-menu__link-text">Menu</span></a></li>

									<?php 
									$fitur = $this->db->get_where('fitur', array('flag' => 1))->result_array();
										foreach ($fitur as $row) {
									?>
										<li class="kt-menu__item <?php if($menu == $row['alias'] ){ echo "kt-menu__item--active"; } ?>" aria-haspopup="true"><a href="<?php echo base_url().'admin/'.$row['alias']; ?>" class="kt-menu__link "><i class="kt-menu__link-icon <?php echo $row['icon'] ?>"></i><span class="kt-menu__link-text"><?php echo $row['fitur'] ?></span></a></li>
									<?php
										}
									 ?>
									


								<?php $get= $this->db->get_where('menu', array('flag' => 1))->result_array();
										foreach ($get as $row) {
											?>

						<li class="kt-menu__item " aria-haspopup="true"><a href="<?php echo base_url().'admin/konten/'.$row['id']; ?>" class="kt-menu__link "><i class="kt-menu__link-icon <?php echo $row['icon'] ?>"></i><span class="kt-menu__link-text"><?php echo $row['nama_menu'] ?></span></a></li>

											<?php
										}
								 ?>
								 <li class="kt-menu__item <?php if($menu == 'media'){ echo "kt-menu__item--active"; } ?>"><a href="<?php echo base_url().'admin/media'; ?>" class="kt-menu__link "><i class="kt-menu__link-icon flaticon-upload-1"></i><span class="kt-menu__link-text">Media</span></a></li>

								 <li class="kt-menu__item <?php if($menu == 'setting'){ echo "kt-menu__item--active"; } ?>" ><a href="<?php echo base_url().'admin/setting'; ?>" class="kt-menu__link "><i class="kt-menu__link-icon flaticon2-gear"></i><span class="kt-menu__link-text">Pengaturan</span></a></li>

							</ul>
						</div>
					</div>

					<!-- end:: Aside Menu -->
				</div>

				<?php 
				$setting = $this->db->get_where('setting', array('id' => 1))->row_array();
				 ?>
				<!-- end:: Aside -->
				<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

					<!-- begin:: Header -->
					<div id="kt_header" class="kt-header kt-grid kt-grid--ver  kt-header--fixed ">

						<!-- begin:: Aside -->
						<div class="kt-header__brand kt-grid__item  " id="kt_header_brand" style="background-color: #FFF;">
							<div class="kt-header__brand-logo">
								<a href="<?php echo base_url().'admin'; ?>">
									<img alt="Logo" src="<?php echo base_url().'assets_foto/'.$setting['logo']; ?>" style="width: 100%"	/>
								</a>
							</div>
						</div>

						<!-- end:: Aside -->

						<!-- begin:: Title -->
						<h3 class="kt-header__title kt-grid__item">
							<?php echo $setting['nama_perusahaan']; ?>
						</h3>

						<!-- end:: Title -->

						<!-- begin: Header Menu -->
						<button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
						<div class="kt-header-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_header_menu_wrapper">
							<div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile  kt-header-menu--layout-default ">
								<ul class="kt-menu__nav ">
									<li class="kt-menu__item  kt-menu__item--active " aria-haspopup="true"><a href="<?php echo base_url().'admin'; ?>" class="kt-menu__link "><span class="kt-menu__link-text">Dashboard</span></a></li>
									<!-- <li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--rel" data-ktmenu-submenu-toggle="click" aria-haspopup="true"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-text">Pages</span><i class="kt-menu__hor-arrow la la-angle-down"></i><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
										<div class="kt-menu__submenu kt-menu__submenu--classic kt-menu__submenu--left">
											<ul class="kt-menu__subnav">
												<li class="kt-menu__item " aria-haspopup="true"><a href="javascript:;" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Create New Post</span></a></li>
												<li class="kt-menu__item " aria-haspopup="true"><a href="#" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Generate Reports</span><span class="kt-menu__link-badge"><span class="kt-badge kt-badge--success">2</span></span></a></li>
												<li class="kt-menu__item  kt-menu__item--submenu" data-ktmenu-submenu-toggle="hover" aria-haspopup="true"><a href="#" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Manage Orders</span><i class="kt-menu__hor-arrow la la-angle-right"></i><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
													<div class="kt-menu__submenu kt-menu__submenu--classic kt-menu__submenu--right">
														<ul class="kt-menu__subnav">
															<li class="kt-menu__item " aria-haspopup="true"><a href="#" class="kt-menu__link "><span class="kt-menu__link-text">Latest Orders</span></a></li>
															<li class="kt-menu__item " aria-haspopup="true"><a href="#" class="kt-menu__link "><span class="kt-menu__link-text">Pending Orders</span></a></li>
															<li class="kt-menu__item " aria-haspopup="true"><a href="#" class="kt-menu__link "><span class="kt-menu__link-text">Processed Orders</span></a></li>
															<li class="kt-menu__item " aria-haspopup="true"><a href="#" class="kt-menu__link "><span class="kt-menu__link-text">Delivery Reports</span></a></li>
															<li class="kt-menu__item " aria-haspopup="true"><a href="#" class="kt-menu__link "><span class="kt-menu__link-text">Payments</span></a></li>
															<li class="kt-menu__item " aria-haspopup="true"><a href="#" class="kt-menu__link "><span class="kt-menu__link-text">Customers</span></a></li>
														</ul>
													</div>
												</li>
											
												<li class="kt-menu__item " aria-haspopup="true"><a href="#" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Register Member</span></a></li>
											</ul>
										</div>
									</li> -->
									
									
								</ul>
							</div>
						</div>

						<!-- end: Header Menu -->

						<!-- begin:: Header Topbar -->
						<div class="kt-header__topbar">

							<!--begin: Search -->
							<div class="kt-header__topbar-item kt-header__topbar-item--search dropdown" id="kt_quick_search_toggle">
								<div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
									<span class="kt-header__topbar-icon"><i class="flaticon2-search-1"></i></span>
								</div>
								<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-lg">
									<div class="kt-quick-search kt-quick-search--inline" id="kt_quick_search_inline">
										<form method="get" class="kt-quick-search__form">
											<div class="input-group">
												<div class="input-group-prepend"><span class="input-group-text"><i class="flaticon2-search-1"></i></span></div>
												<input type="text" class="form-control kt-quick-search__input" placeholder="Search...">
												<div class="input-group-append"><span class="input-group-text"><i class="la la-close kt-quick-search__close"></i></span></div>
											</div>
										</form>
										<div class="kt-quick-search__wrapper kt-scroll" data-scroll="true" data-height="300" data-mobile-height="200">
										</div>
									</div>
								</div>
							</div>

							<!--end: Search -->

							<!--begin: Notifications -->
							<div class="kt-header__topbar-item dropdown">
								<div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
									<span class="kt-header__topbar-icon kt-header__topbar-icon--success"><i class="flaticon2-bell-alarm-symbol"></i></span>
									<span class="kt-hidden kt-badge kt-badge--danger"></span>
								</div>
								<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-xl">
									<form>

										<!--begin: Head -->
										<div class="kt-head kt-head--skin-dark kt-head--fit-x kt-head--fit-b" style="background-image: url(<?php echo base_url().'assets/media/misc/bg-1.jpg';?>)">
											<h3 class="kt-head__title">
												User Notifications
												&nbsp;
												<span class="btn btn-success btn-sm btn-bold btn-font-md">23 new</span>
											</h3>
											<ul class="nav nav-tabs nav-tabs-line nav-tabs-bold nav-tabs-line-3x nav-tabs-line-success kt-notification-item-padding-x" role="tablist">
												<li class="nav-item">
													<a class="nav-link active show" data-toggle="tab" href="#topbar_notifications_notifications" role="tab" aria-selected="true">Alerts</a>
												</li>
												<li class="nav-item">
													<a class="nav-link" data-toggle="tab" href="#topbar_notifications_events" role="tab" aria-selected="false">Events</a>
												</li>
												<li class="nav-item">
													<a class="nav-link" data-toggle="tab" href="#topbar_notifications_logs" role="tab" aria-selected="false">Logs</a>
												</li>
											</ul>
										</div>

										<!--end: Head -->
										<div class="tab-content">
											<div class="tab-pane active show" id="topbar_notifications_notifications" role="tabpanel">
												<div class="kt-notification kt-margin-t-10 kt-margin-b-10 kt-scroll" data-scroll="true" data-height="300" data-mobile-height="200">
													<a href="#" class="kt-notification__item">
														<div class="kt-notification__item-icon">
															<i class="flaticon2-line-chart kt-font-success"></i>
														</div>
														<div class="kt-notification__item-details">
															<div class="kt-notification__item-title">
																New order has been received
															</div>
															<div class="kt-notification__item-time">
																2 hrs ago
															</div>
														</div>
													</a>
												</div>
											</div>
											<div class="tab-pane" id="topbar_notifications_events" role="tabpanel">
												<div class="kt-notification kt-margin-t-10 kt-margin-b-10 kt-scroll" data-scroll="true" data-height="300" data-mobile-height="200">
													<a href="#" class="kt-notification__item">
														<div class="kt-notification__item-icon">
															<i class="flaticon2-psd kt-font-success"></i>
														</div>
														<div class="kt-notification__item-details">
															<div class="kt-notification__item-title">
																New report has been received
															</div>
															<div class="kt-notification__item-time">
																23 hrs ago
															</div>
														</div>
													</a>
													
												</div>
											</div>
											<div class="tab-pane" id="topbar_notifications_logs" role="tabpanel">
												<div class="kt-grid kt-grid--ver" style="min-height: 200px;">
													<div class="kt-grid kt-grid--hor kt-grid__item kt-grid__item--fluid kt-grid__item--middle">
														<div class="kt-grid__item kt-grid__item--middle kt-align-center">
															All caught up!
															<br>No new notifications.
														</div>
													</div>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>

							<!--end: Notifications -->

						



							<!--begin: User bar -->
							<div class="kt-header__topbar-item kt-header__topbar-item--user">
								<div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
									<span class="kt-hidden kt-header__topbar-welcome">Hi,</span>
									<span class="kt-hidden kt-header__topbar-username">Nick</span>
									<img class="kt-hidden" alt="Pic" src="<?php echo base_url().'assets/media/users/300_21.jpg';?>" />
									<span class="kt-header__topbar-icon kt-hidden-"><i class="flaticon2-user-outline-symbol"></i></span>
								</div>
								<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-xl">

									<!--begin: Head -->
									<div class="kt-user-card kt-user-card--skin-dark kt-notification-item-padding-x" style="background-image: url(<?php echo base_url().'assets/media/misc/bg-1.jpg';?>)">
										<div class="kt-user-card__avatar">
											<img class="kt-hidden" alt="Pic" src="<?php echo base_url().'assets/media/users/300_25.jpg';?>" />

											
											<!-- <span class="kt-badge kt-badge--lg kt-badge--rounded kt-badge--bold kt-font-success">S</span> -->
										</div>
										<div class="kt-user-card__name">
											<?php echo $this->session->userdata('nama'); ?>
										</div>
										<!-- <div class="kt-user-card__badge">
											<span class="btn btn-success btn-sm btn-bold btn-font-md">23 messages</span>
										</div> -->
									</div>

									<!--end: Head -->

									<!--begin: Navigation -->
									<div class="kt-notification">
										<a href="#" class="kt-notification__item">
											<div class="kt-notification__item-icon">
												<i class="flaticon2-calendar-3 kt-font-success"></i>
											</div>
											<div class="kt-notification__item-details">
												<div class="kt-notification__item-title kt-font-bold">
													My Profile
												</div>
												<div class="kt-notification__item-time">
													Account settings and more
												</div>
											</div>
										</a>
										<!-- <a href="#" class="kt-notification__item">
											<div class="kt-notification__item-icon">
												<i class="flaticon2-mail kt-font-warning"></i>
											</div>
											<div class="kt-notification__item-details">
												<div class="kt-notification__item-title kt-font-bold">
													My Messages
												</div>
												<div class="kt-notification__item-time">
													Inbox and tasks
												</div>
											</div>
										</a>
										<a href="#" class="kt-notification__item">
											<div class="kt-notification__item-icon">
												<i class="flaticon2-rocket-1 kt-font-danger"></i>
											</div>
											<div class="kt-notification__item-details">
												<div class="kt-notification__item-title kt-font-bold">
													My Activities
												</div>
												<div class="kt-notification__item-time">
													Logs and notifications
												</div>
											</div>
										</a>
										<a href="#" class="kt-notification__item">
											<div class="kt-notification__item-icon">
												<i class="flaticon2-hourglass kt-font-brand"></i>
											</div>
											<div class="kt-notification__item-details">
												<div class="kt-notification__item-title kt-font-bold">
													My Tasks
												</div>
												<div class="kt-notification__item-time">
													latest tasks and projects
												</div>
											</div>
										</a>
										<a href="#" class="kt-notification__item">
											<div class="kt-notification__item-icon">
												<i class="flaticon2-cardiogram kt-font-warning"></i>
											</div>
											<div class="kt-notification__item-details">
												<div class="kt-notification__item-title kt-font-bold">
													Billing
												</div>
												<div class="kt-notification__item-time">
													billing & statements <span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill kt-badge--rounded">2 pending</span>
												</div>
											</div>
										</a> -->
										<div class="kt-notification__custom kt-space-between">
											<a href="<?php echo base_url().'auth/keluar'; ?>" target="_blank" class="btn btn-label btn-label-brand btn-sm btn-bold">Sign Out</a>
											<!-- <a href="demo6/custom/user/login-v2.html" target="_blank" class="btn btn-clean btn-sm btn-bold">Upgrade Plan</a> -->
										</div>
									</div>

									<!--end: Navigation -->
								</div>
							</div>

							<!--end: User bar -->

							
						</div>

						<!-- end:: Header Topbar -->
					</div>

					<!-- end:: Header -->