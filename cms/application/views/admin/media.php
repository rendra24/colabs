<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
						<div class="kt-subheader-search">
							<h3 class="kt-subheader-search__title">
								Cari Media
								<span class="kt-subheader-search__desc">berdasarkan</span>
							</h3>
							<form class="kt-form">
								<div class="kt-grid kt-grid--desktop kt-grid--ver-desktop">
									<div class="kt-grid__item kt-grid__item--middle">
										<div class="row kt-margin-r-10">
											<div class="col-lg-6">
												<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
													<input type="text" class="form-control form-control-pill" placeholder="Nama Menu">
													<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i class="la la-puzzle-piece"></i></span></span>
												</div>
											</div>
											<div class="col-lg-3">
												<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
													<input type="text" class="form-control form-control-pill" placeholder="From">
													<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i class="la la-calendar-check-o"></i></span></span>
												</div>
											</div>
											<div class="col-lg-3">
												<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
													<input type="text" class="form-control form-control-pill" placeholder="To">
													<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i class="la la-calendar-check-o"></i></span></span>
												</div>
											</div>
										</div>
									</div>
									<div class="kt-grid__item kt-grid__item--middle">
										<div class="kt-margin-top-20 kt--visible-tablet-and-mobile"></div>
										<button type="button" class="btn btn-pill btn-upper btn-bold btn-font-sm kt-subheader-search__submit-btn">Cari</button>
									</div>
								</div>
							</form>
						</div>

						<!-- begin:: Content -->

						<!-- begin:: Content -->
						<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

							<div class="row">
								<div class="col-xl-12">

									<!--begin:: Widgets/Sale Reports-->
									<div class="kt-portlet kt-portlet--tabs kt-portlet--height-fluid">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													Media Website
												</h3>
											</div>
												<div class="kt-portlet__head-toolbar" style="padding-top: 15px;padding-bottom: 15px;">
												<a href="#" class="btn btn-label-brand btn-bold btn-sm" data-toggle="modal" data-target="#exampleModal">Upload Baru &nbsp;<i class="flaticon2-plus" style="font-size: 10px;"></i></a>
											</div>
										</div>
										<div class="kt-portlet__body">
											<div class="row" id="load_image">
												<?php foreach ($media as $row) {
												?>
												<div class="col-2"><img src="<?php echo base_url().'media/'.$row['media']; ?>" class="img-responsive" style="width: 100%;height: auto;"></div>
												<?php
												} ?>
												
											</div>
										</div>
									</div>

								</div>
							</div>
						
						</div>

					</div>


<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
  	<form class="formkonten">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambahkan Media</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <div class="row">
       	<div class="col-md-12">

       		 <div class="form-group">
              <label>Judul Gambar<span class="text-danger">*</span></label>
              <input type="text" name="title" class="form-control" value="" />
            </div>

             <div class="form-group">
            <label>Media <span class="text-danger">*</span></label>
            <input type="file" name="userfile" class="form-control" />
          </div>

       	</div>
       </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Upload Media</button>
      </div>
    </div>
    </form>
  </div>
</div>

<script type="text/javascript">
	   $(".formkonten").submit(function(event){
    event.preventDefault();
    

    var formData = new FormData(this);

    $.ajax({
      type: "POST",
      url: "<?php echo base_url().'admin/simpan_media'; ?>",
      data:formData,
      dataType: 'json',
      processData:false,
      contentType:false,
      cache:false,
      async:false,
      success: function(data) {

        if(data.success == true)
        {
         	$('#load_image').append('<div class="col-2"><img src="'+data.new_image+'" class="img-responsive" style="width: 100%;height: auto;"></div>');
         	$('.form-control').val('');
         	$('#exampleModal').modal('hide');	
        }else{
           swal.fire({
  icon: 'error',
  title: 'Gagal',
  text: data.message
});
        }
      }
    });

  });
</script>
					

		

	
