<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
						<div class="kt-subheader-search">
							<h3 class="kt-subheader-search__title">
								Cari Menu
								<span class="kt-subheader-search__desc"></span>
							</h3>
							<form class="kt-form">
								<div class="kt-grid kt-grid--desktop kt-grid--ver-desktop">
									<div class="kt-grid__item kt-grid__item--middle">
										<div class="row kt-margin-r-10">
											<div class="col-lg-6">
												<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
													<input type="text" class="form-control form-control-pill" placeholder="Nama Menu">
													<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i class="la la-puzzle-piece"></i></span></span>
												</div>
											</div>
											<div class="col-lg-3">
												<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
													<input type="text" class="form-control form-control-pill" placeholder="From">
													<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i class="la la-calendar-check-o"></i></span></span>
												</div>
											</div>
											<div class="col-lg-3">
												<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
													<input type="text" class="form-control form-control-pill" placeholder="To">
													<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i class="la la-calendar-check-o"></i></span></span>
												</div>
											</div>
										</div>
									</div>
									<div class="kt-grid__item kt-grid__item--middle">
										<div class="kt-margin-top-20 kt--visible-tablet-and-mobile"></div>
										<button type="button" class="btn btn-pill btn-upper btn-bold btn-font-sm kt-subheader-search__submit-btn">Cari</button>
									</div>
								</div>
							</form>
						</div>

						<!-- begin:: Content -->

						<!-- begin:: Content -->
						<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

							<div class="row">
								<div class="col-xl-12">

									<!--begin:: Widgets/Sale Reports-->
									<div class="kt-portlet kt-portlet--tabs kt-portlet--height-fluid">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													Menu
												</h3>
											</div>
												<div class="kt-portlet__head-toolbar" style="padding-top: 15px;padding-bottom: 15px;">
												<a href="<?php echo base_url().'admin/form_menu/'; ?>" class="btn btn-label-brand btn-bold btn-sm">Buat Baru &nbsp;<i class="flaticon2-plus" style="font-size: 10px;"></i></a>
											</div>
										</div>
										<div class="kt-portlet__body">

									

													<div class="kt-widget11">
														<div class="table-responsive">
															<table class="table">
																<thead>
																	<tr>
																		<td style="width:1%">#</td>
																		<td style="width:20%">Menu</td>
																		<td style="width:24%">Total Konten</td>
																		<td style="width:35%">Keterangan</td>
																		<td style="width:10%">Status</td>
																		<td style="width:15%" class="kt-align-right">Action</td>
																	</tr>
																</thead>
																<tbody>
																	<?php 
																	$no = 1;
																	foreach ($menus as $row) {

																	?>


																	<tr>
																		<td>
																			<span>1</span>
																		</td>
																		<td>
																			<a href="#" class="kt-widget11__title"><?php echo $row['nama_menu']; ?></a>
																		</td>
																		<td>19,200</td>
																		<td><?php echo $row['keterangan']; ?></td>
																		<td>
																			<?php if($row['flag'] == 1){ ?>
																			<span class="kt-badge kt-badge--inline kt-badge--brand">Aktif</span>
																		<?php }else{ ?>
																			<span class="kt-badge kt-badge--inline kt-badge--danger">Non Aktif</span>
																		<?php } ?>
																		</td>
																		<td class="kt-align-right kt-font-brand kt-font-bold">
																			<a href="<?php echo base_url().'admin/form_menu/'.$row['id']; ?>" class="btn btn-sm btn-clean btn-icon mr-2" title="Edit details">                            <span class="svg-icon svg-icon-md">                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">                                        <rect x="0" y="0" width="24" height="24"></rect>                                        <path d="M8,17.9148182 L8,5.96685884 C8,5.56391781 8.16211443,5.17792052 8.44982609,4.89581508 L10.965708,2.42895648 C11.5426798,1.86322723 12.4640974,1.85620921 13.0496196,2.41308426 L15.5337377,4.77566479 C15.8314604,5.0588212 16,5.45170806 16,5.86258077 L16,17.9148182 C16,18.7432453 15.3284271,19.4148182 14.5,19.4148182 L9.5,19.4148182 C8.67157288,19.4148182 8,18.7432453 8,17.9148182 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000000, 10.707409) rotate(-135.000000) translate(-12.000000, -10.707409) "></path>                                        <rect fill="#000000" opacity="0.3" x="5" y="20" width="15" height="2" rx="1"></rect>                                    </g>                                </svg>                            </span>                        </a>
																		</td>
																	</tr>
																	<?php
																	$no++;
																	} ?>
																</tbody>
															</table>
														</div>
														<!-- <div class="kt-widget11__action kt-align-right">
															<button type="button" class="btn btn-label-brand btn-bold btn-sm">Import Report</button>
														</div> -->
													</div>


										</div>
									</div>

								</div>
							</div>
						
						</div>

					</div>

					

		

	
