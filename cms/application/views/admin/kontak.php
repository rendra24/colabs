<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
						<div class="kt-subheader-search">
							<h3 class="kt-subheader-search__title">
								Cari Kontak
								<span class="kt-subheader-search__desc"></span>
							</h3>
							<form class="kt-form">
								<div class="kt-grid kt-grid--desktop kt-grid--ver-desktop">
									<div class="kt-grid__item kt-grid__item--middle">
										<div class="row kt-margin-r-10">
											<div class="col-lg-6">
												<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
													<input type="text" class="form-control form-control-pill" placeholder="Nama Menu">
													<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i class="la la-puzzle-piece"></i></span></span>
												</div>
											</div>
											<div class="col-lg-3">
												<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
													<input type="text" class="form-control form-control-pill" placeholder="From">
													<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i class="la la-calendar-check-o"></i></span></span>
												</div>
											</div>
											<div class="col-lg-3">
												<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
													<input type="text" class="form-control form-control-pill" placeholder="To">
													<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i class="la la-calendar-check-o"></i></span></span>
												</div>
											</div>
										</div>
									</div>
									<div class="kt-grid__item kt-grid__item--middle">
										<div class="kt-margin-top-20 kt--visible-tablet-and-mobile"></div>
										<button type="button" class="btn btn-pill btn-upper btn-bold btn-font-sm kt-subheader-search__submit-btn">Cari</button>
									</div>
								</div>
							</form>
						</div>

						<!-- begin:: Content -->

						<!-- begin:: Content -->
						<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

							<div class="row">
								<div class="col-xl-12">

									<!--begin:: Widgets/Sale Reports-->
									<div class="kt-portlet kt-portlet--tabs kt-portlet--height-fluid">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													Kontak Kami
												</h3>
											</div>
												<!-- <div class="kt-portlet__head-toolbar" style="padding-top: 15px;padding-bottom: 15px;">
												<a href="<?php //echo base_url().'admin/form_menu/'; ?>" class="btn btn-label-brand btn-bold btn-sm">Buat Baru &nbsp;<i class="flaticon2-plus" style="font-size: 10px;"></i></a>
											</div> -->
										</div>
										<div class="kt-portlet__body">

									

													<div class="kt-widget11">
														<div class="table-responsive">
															<table class="table">
																<thead>
																	<tr>
																		<td style="width:1%">#</td>
																		<td style="width:15%">Nama</td>
																		<td style="width:15%">Email</td>
																		<td style="width:24%">Subject</td>
																		<td style="width:45%">Pesan</td>
																	</tr>
																</thead>
																<tbody>
																	<?php 
																	$no = 1;
																	foreach ($kontak as $row) {

																	?>


																	<tr>
																		<td>
																			<span><?php echo $no; ?></span>
																		</td>
																		<td>
																			<?php echo $row['nama']; ?>
																		</td>
																		<td><?php echo $row['email']; ?></td>
																		<td><?php echo $row['subject']; ?></td>
																		<td>
																			<?php echo $row['pesan']; ?>
																		</td>
																	</tr>
																	<?php
																	$no++;
																	} ?>
																</tbody>
															</table>
														</div>
														<!-- <div class="kt-widget11__action kt-align-right">
															<button type="button" class="btn btn-label-brand btn-bold btn-sm">Import Report</button>
														</div> -->
													</div>


										</div>
									</div>

								</div>
							</div>
						
						</div>

					</div>

					

		

	
